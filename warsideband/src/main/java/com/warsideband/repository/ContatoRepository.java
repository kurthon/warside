package com.warsideband.repository;

import org.springframework.data.repository.CrudRepository;

import com.warsideband.models.Contato;


public interface ContatoRepository extends CrudRepository<Contato, String>{
	Contato findByCodigo(long codigo);


}
