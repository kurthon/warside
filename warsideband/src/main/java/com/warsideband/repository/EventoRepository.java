package com.warsideband.repository;

import org.springframework.data.repository.CrudRepository;
import com.warsideband.models.Eventos;

public interface EventoRepository extends CrudRepository<Eventos, String>{
	Eventos findByCodigo(long codigo);
	

	
}
